-- https://www.datos.gov.py/dataset


-- Listado de tablas a consultar
SELECT table_name 
FROM information_schema.tables 
WHERE table_schema='public' 
AND table_type='BASE TABLE';


-- Consulta a una tabla (aleatoria) - Ejemplo
SELECT * FROM "32da0ea6-3485-49f7-882f-fa2c48b79e7a";


